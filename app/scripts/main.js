Modernizr.addTest('scrollbarwidth', function () {
  var rules, head, root, element, scrollbarWidth, style;
  
  
  rules = ['#modernizr-scrollbarwidth{ width: 100px; height: 100px; overflow: scroll; position: absolute; top: -9999px; }'];
  head = document.getElementsByTagName('head')[0] || (function () {
    return document.documentElement.appendChild(document.createElement('head'));
  }());
  root = document.body || (function () {
    return document.documentElement.appendChild(document.createElement('body'));
  }());
  element = document.createElement('div'),
  style = document.createElement('style');
  
  style.type = "text/css";
  if (style.styleSheet) { 
    style.styleSheet.cssText = rules.join(''); 
  } else {
    style.appendChild(document.createTextNode(rules.join(''))); 
  }
  head.appendChild(style);
  
  element.id = "modernizr-scrollbarwidth";
  root.appendChild(element);
  scrollbarWidth = element.offsetWidth - element.clientWidth; // http://davidwalsh.name/detect-scrollbar-width
    
  head.removeChild(style);
  root.removeChild(element);
  
  return scrollbarWidth > 0;
});

if (Modernizr.scrollbarwidth) {
  var rootEl = document.getElementsByTagName('html')[0];
  rootEl.className = rootEl.className + ' scrollbarwidth';
}

function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
  {
    return 'ios';

  }
  else if( userAgent.match( /Android/i ) )
  {

    return 'android';
  }
  else
  {
    return 'unknown';
  }
}

document.getElementsByTagName('html')[0].className = document.getElementsByTagName('html')[0].className + ' ' + getMobileOperatingSystem();
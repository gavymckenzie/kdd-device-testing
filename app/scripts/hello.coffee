nav =
  init: ->
    $('.article').eq(0).addClass 'active'
    @bind()
  bind: ->
    $('[role="navigation"] a[href^=#]').on 'click', @navigate
    $('.article-title').on 'click', @showContent

  navigate: (e) ->
    e.preventDefault()
    $el = $ @
    $article = $ $el.attr('href')

    $('.article').removeClass 'active'
    $('.article').removeClass 'show-content'
    $article.addClass 'active'

  showContent: (e) ->
    e.preventDefault()
    $el = $ @

    $el.closest('.article').addClass 'show-content'

nav.init()